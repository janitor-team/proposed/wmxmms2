/*
 * wmxmms2
 * Copyright (c)2005 Remy Bosch (remybosch@zonnet.nl)
 * 
 * This software covered by the GPL.  See COPYING file for details.
 *
 * xmms2cif.h
 *
 */
#ifndef __XMMS2CIF_H__
#define __XMMS2CIF_H__
#include <xmmsclient/xmmsclient.h>
#include <string.h>
volatile unsigned int STATE;
#define S_IDLE  0
#define S_PLAY  1
#define S_PAUSE 2
#define S_DOWN  3
#define S_ERROR 99

struct numberinfo {
	uint length;
	uint played;
	char title_info[100];
};
typedef struct numberinfo numberinfo;
numberinfo numinfo;

char songname[100];

void xmms2connect (void);
void play_cmd(void);
void stop_cmd(void);
void randomize_cmd(void);
void pause_cmd(void);
void jmp_cmd(int places);
void prev_cmd(void);
void next_cmd(void);
/*void seek_cmd(xmmsc_connection_t srvr);*/
void disconnect_server(void);
void check_status(void);
void get_songdata(void);
void playtime(void);
void title_info(void);
void fwd_cmd(void);
void rwnd_cmd(void);
void timelaps(int delta);
/*void media_info(void);*/


xmmsc_connection_t *srvr;

#endif
