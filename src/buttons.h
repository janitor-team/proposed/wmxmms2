/*
 * wmxmms2
 * Copyright (c)2005 Remy Bosch (remybosch@zonnet.nl)
 * 
 * This software covered by the GPL.  See COPYING file for details.
 *
 * buttons.h
 *
 */
#ifndef __BUTTONS_H__
#define __BUTTONS_H__

/* X11 extentions */
#include <X11/Xlib.h>
#include <X11/xpm.h>
#include <X11/extensions/shape.h>
#include "wmgeneral.h"
#include "xmms2cif.h"

/* xmms2clientlib */
#include <xmmsclient/xmmsclient.h>

/* X11 wheel button codes (for wheel mice) */
#define BUTTON_WHEEL_UP 4
#define BUTTON_WHEEL_DOWN 5

/* Button defs */
#define PLAY   0
#define PAUSE  1
#define PREV   2
#define NEXT   3
#define RWND   4
#define FWD    5
#define RANDOM 6
#define STOP   7

/* Button stuff */
void initButtons (void);
void buttonReleaseHandler(int butnum);
int buttonPressHandler(const XEvent *Event);
void stateUpdate(unsigned int NewState);

/* Button stuff */
void buttonDown(int i);
void buttonUp(int i);

/*extern void seek_cmd(xmmsc_connection_t srvr);*/
extern void disconnect_server(void);

#endif
