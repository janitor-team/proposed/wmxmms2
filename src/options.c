/*
 * wmxmms2
 * Copyright (c)2005 Remy Bosch (remybosch@zonnet.nl)
 * 
 * This software covered by the GPL.  See COPYING file for details.
 *
 * options.c
 *
 */
#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include "options.h"

void incompatArg(int *index, int c, char *optarg);
void showoption(const char *str , char *option);
void showHelp(void);

static struct option options[]={
	{ "speed", required_argument, 0,'s' },
	{ "colour", required_argument, 0,'c' },
	{ "url", required_argument, 0, 'u' },
	{ "version", no_argument, 0,'v' },
	{ "help", no_argument, 0,'h' },
	{ 0, 0, 0, 0 }
};

void optionHandler(int argc, char **argv){
	int c;
	int index = 0;
	int itmp;
	char *tail;

	while ((c=(getopt_long (argc, argv, "u:s:c:hv", options, &index))) != -1){
		int errno=0;
		switch (c){
			case 's':
				itmp = strtol(optarg,&tail,0);
				if (errno || itmp<1 ){
					incompatArg(&index, c, optarg);
					showHelp();
					exit(1);
				}
				varoptions.speed=itmp;
				break;
			case 'u':
				varoptions.ip_address=optarg;
				break;
			case 'c':
				varoptions.colour=optarg;
				break;
			case 'v':
				printf("Wmxmms version %s\n",VERSION);
				exit(0);
				break;
			case 'h':
				showHelp();
				exit(0);
		}
	}
}

void incompatArg(int *index, int c, char *optarg){
	if (index==0)
	fprintf(stderr,	"Incompatible argument for -%c:\n%s\n",c ,optarg);
	else
	fprintf(stderr,	"Incompatible argument for --%s:\n%s\n",
			options[*index].name,
			optarg);
}

void showoption(const char *str , char *option){
	printf("Option --%s ",str);
	if (option!=NULL)
		printf(" with %s as argument",option);
	printf(".\n");
}

void showHelp(void){
	printf("\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n\n",
"Copyright (C) 2005 Remy Bosch",
"Wmxmms is released under the terms of the GPL v2",
"Usage: wmxmms [OPTIONS]",
"Options for wmxmms2:",
"-s --speed [ms/char]   Scrollspeed for text ( gets divided by ten :P )",
"-c --colour [white]    Give colour for text",
"-u --url               Address of server",
"-v --version           Version info",
"-h --help              This text"
	);
}
