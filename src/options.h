/*
 * wmxmms2
 * Copyright (c)2005 Remy Bosch (remybosch@zonnet.nl)
 * 
 * This software covered by the GPL.  See COPYING file for details.
 *
 * options.h
 *
 */
#ifndef _OPTION_H
#define _OPTION_H
struct varList{
	unsigned int speed;
	char *colour;
	char *ip_address;
} varoptions;

void optionHandler(int argc, char **argv);
#endif
